FROM node:14.16.1-alpine

WORKDIR /usr/src/app

ARG NODE_ENV
ENV NODE_ENV $NODE_ENV

COPY package*.json /usr/src/app/
RUN npm install

COPY . /usr/src/app

RUN npm run build

EXPOSE $PORT
CMD [ "npm", "start" ]
