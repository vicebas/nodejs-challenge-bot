import { createServer, IncomingMessage, ServerResponse} from "http";
import  dotenv from "dotenv";
import QueueService from "./queue.service";
import BotService from "./bot.service";

dotenv.config();

const PORT = process.env.PORT || 8080

const server = createServer(  (req: IncomingMessage, res: ServerResponse) =>{
    switch (req.url) {
    case '/message': {
      if (req.method === 'POST') {
        let body = "";
        req.on("data", (chunk) => {
          body += chunk;
        });
        req.on("end", async () => {
          const msg = JSON.parse(body);
          BotService.processCommand(msg.text, msg.channel);
          res.write("QUEUED");
          res.end();
        });
      }
      break;
    }
    default: {
      res.statusCode = 404;
      res.end();
    }
  }
});



server.listen(PORT,  () => {
    console.log(`Server is running on http://localhost:${PORT}`);
});



