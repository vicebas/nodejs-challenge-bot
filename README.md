Small bot capable of getting stock prices for [NodeJS Chat](https://gitlab.com/vicebas/nodejs-challenge)

## Developing

### Configuring node dependencies

Use the following procedure to setup the dev environment:

- Run `npm install`

### Running locally

Run the following commands:

```bash
npm start
```

## Languages & tools

### Node.js base dependencies

- [Typescript](https://www.typescriptlang.org/) Superset of javascript that compiles to plain javascript
- [RabbitMQ](https://www.rabbitmq.com/) RabbitMQ is a message broker:
