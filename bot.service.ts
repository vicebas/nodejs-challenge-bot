import axios  from "axios";
import csv from 'csvtojson'
import QueueService from "./queue.service";
export default class BotService {
    public static async processCommand(msg : string, channel: string){
        let returnMsg = 'unknown Command';
        switch(msg.split("=")[0]){
            case "stock" :{
                const data: any =  await BotService.getStockData(msg.split("=")[1]);
                if (data["Close"] == "N/D"){
                  returnMsg = `${data["Symbol"]} stock not found`;
                }else{
                  returnMsg = `${data["Symbol"]} quote is ${data["Close"]} per share`;
                }
                break;
            }
        }
        QueueService.sendMessage(
          JSON.stringify({
            username: "finanbot",
            text: returnMsg,
            channel
          })
        );
    }

    public static async getStockData(stock:string){
        const stockData = (await axios.get(
          `https://stooq.com/q/l/?s=${stock}&f=sd2t2ohlcv&h&e=csv`
        )).data;
        return new Promise(function(resolve, reject) {
            csv({
                noheader:false,
            })
            .fromString(stockData)
            .then((csvRow: any)=>{ 
                resolve(csvRow[0]) // => [["1","2","3"], ["4","5","6"], ["7","8","9"]]
            })

        // executor (the producing code, "singer")
        });
    }
}